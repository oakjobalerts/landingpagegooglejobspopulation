package com.project.application;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import com.project.controller.HomeController;

@SpringBootApplication
@ComponentScan(basePackages = { "com.project" })
@PropertySource({ "classpath:/project.properties" })
@EnableMongoRepositories(basePackages = { "com.project" })
public class Application implements org.springframework.boot.CommandLineRunner {
    @Autowired
    HomeController homeController;

    public Application() {
    }

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }

    @Bean
    public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory emf) {
        HibernateJpaSessionFactoryBean factory = new HibernateJpaSessionFactoryBean();
        factory.setEntityManagerFactory(emf);
        return factory;
    }

    public void run(String... args) throws Exception {

        System.out.println("starting project");
        homeController.startProcess();
    }
}
