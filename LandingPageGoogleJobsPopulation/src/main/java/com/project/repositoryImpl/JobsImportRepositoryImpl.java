package com.project.repositoryImpl;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.project.model.JobsDetailMongoDbModel;
import com.project.model.JobsModel;
import com.project.repository.JobsImportRepository;

@Repository
public class JobsImportRepositoryImpl implements JobsImportRepository {

	@Autowired
	SessionFactory sessionFactory;

	@Value("${db.table.name}")
	String tableName;

	@Value("${db.select.query}")
	String query;

	@Value("${db.query.where.condition}")
	String whereCondition;

	public JobsImportRepositoryImpl() {
	}

	@Transactional
	@Override
	public List<JobsModel> getJobsFromDbWithStartId(int start, int limit) {

		try {

			Query sqlQuery = getSessionFactory().createSQLQuery(query + " " + tableName + " " + whereCondition + " limit " + start + ", " + limit)
					.setResultTransformer(Transformers.aliasToBean(JobsModel.class));

			return sqlQuery.list();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<JobsDetailMongoDbModel> convertJobsDbModelsIntoMongoDbJobsModels(List<JobsModel> jobsModels) {

		Type type = new TypeToken<List<JobsDetailMongoDbModel>>() {
		}.getType();

		Gson gson = new GsonBuilder()

				.setDateFormat(0, 0)
				.disableHtmlEscaping()
				.create();

		List<JobsDetailMongoDbModel> jobsDetailMongoDbModels = gson.fromJson(gson.toJson(jobsModels), type);

		return jobsDetailMongoDbModels;
	}

	@Override
	public List<JobsDetailMongoDbModel> combineTitleLocationForUrlBeautify(List<JobsDetailMongoDbModel> jobsDetailMongoDbModels) {

		List<JobsDetailMongoDbModel> jobsDetailMongoDbModelsUpdated = new ArrayList<JobsDetailMongoDbModel>();

		for (JobsDetailMongoDbModel jobs : jobsDetailMongoDbModels) {
			// job-desc-id-15032118-q-Sales%20Representative%20-%20Security-l-Bohemia%20,%20NY-jobs
			String beautifyUrl = "job-desc-id-";
			beautifyUrl = beautifyUrl + nullCheck(replaceCharactersAndConvertToLowerCase("" + jobs.getId().intValue()));

			beautifyUrl = beautifyUrl + "-q-" + nullCheck(replaceCharactersAndConvertToLowerCase(jobs.getTitle()));
			beautifyUrl = beautifyUrl + "-l-" + nullCheck(replaceCharactersAndConvertToLowerCase(jobs.getCity() + " , " + jobs.getState()));

			beautifyUrl = beautifyUrl + "-jobs.html";

			if (beautifyUrl != null && !beautifyUrl.equals("")) {

				JobsDetailMongoDbModel detailMongoDbModel = new Gson().fromJson(new Gson().toJson(jobs), JobsDetailMongoDbModel.class);
				detailMongoDbModel.setBeautifyUrl(beautifyUrl);
				jobsDetailMongoDbModelsUpdated.add(detailMongoDbModel);
			}
		}

		return jobsDetailMongoDbModelsUpdated;

	}

	public String nullCheck(String value) {

		return value == null ? "" : value;
	}

	String replaceCharactersAndConvertToLowerCase(String value) {

		value = value
				.toLowerCase()
				.replace(",", "")
				.replace("*", "")
				.replace("/", "")
				.replace("?", "")
				.replace("&", "")
				.replace("<", "")
				.replace(">", "")
				.replace("\"", "")
				.replace("(", "")
				.replace(")", "")
				.replace("$", "")
				.replace(":", "")
				.replace("'", "")
				.replace("!", "")
				.replace("%", "")
				.replace("-", "");

		try {
			return URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}

	public Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
