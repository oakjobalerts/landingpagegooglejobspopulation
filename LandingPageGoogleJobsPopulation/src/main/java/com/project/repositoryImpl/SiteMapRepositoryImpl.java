package com.project.repositoryImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.project.adaptors.EscapeHandler;
import com.project.model.JobsDetailMongoDbModel;
import com.project.model.sitemap.UrlModel;
import com.project.repository.SiteMapRepository;
import com.sun.xml.bind.marshaller.DataWriter;

@Repository
public class SiteMapRepositoryImpl implements SiteMapRepository {

    @Value("${sitemap.folder.path}")
    String folderPath;

    @Value("${sitemap.outer.start.element.name}")
    String outerStartElement;

    @Value("${sitemap.url.change.frequency}")
    String changeFrequency;

    @Value("${sitemap.url.domain}")
    String urlDomain;

    @Value("${sitemap.url.after.domain.path}")
    String urlPath;

    @Value("${sitemap.url.float.priority}")
    float sitemapUrlPriority;

    @Override
    public DataWriter writeStartElement() {

        // Fri, 10 Dec 2008 22:49:39 GMT
        DateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM YYYY hh:mm:ss z");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

        DataWriter dataWriter = null;

        try {
            PrintWriter writer = null;
            File file = new File(folderPath + "sitemap.xml");
            writer = new PrintWriter(new FileWriter(file));
            dataWriter = new DataWriter(writer, "utf-8", new EscapeHandler());
            dataWriter.startDocument();
            dataWriter.characters("<"
                    + outerStartElement
                    + " xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'"
                    + " xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'"
                    + " xsi:schemaLocation='http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'"
                    + ">");

            dataWriter.endDocument();
            dataWriter.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dataWriter;
    }

    @Override
    public synchronized void writeXmlEndElement(DataWriter dataWriter) {

        try {

            dataWriter.characters("</" + outerStartElement + ">");
            dataWriter.endDocument();
            dataWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Marshaller createMarshaller() {

        Marshaller marshaller = null;

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(UrlModel.class);

            marshaller = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return marshaller;
    }

    @Override
    public synchronized void writeFragmentedMarshlingForJujuExportModel(UrlModel urlModel, DataWriter dataWriter, Marshaller marshaller) {

        try {
            marshaller.marshal(urlModel, dataWriter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<UrlModel> convertJobModelINtoSitemapUrlModel(List<JobsDetailMongoDbModel> jobsDetailMongoDbModelList) {

        List<UrlModel> urlModelList = new ArrayList<UrlModel>();

        DateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");

        for (JobsDetailMongoDbModel jobsModel : jobsDetailMongoDbModelList) {

            UrlModel urlModel = new UrlModel();

            urlModel.setChangefreq(changeFrequency);
            urlModel.setLastmod(formatter.format(new Date()));
            urlModel.setLoc(urlDomain + jobsModel.getBeautifyUrl());
            urlModel.setPriority(sitemapUrlPriority);
            urlModelList.add(urlModel);
        }

        return urlModelList;

    }
}
