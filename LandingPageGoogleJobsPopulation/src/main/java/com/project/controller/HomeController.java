package com.project.controller;

import java.util.List;

import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Controller;

import com.project.model.JobsDetailMongoDbModel;
import com.project.model.JobsModel;
import com.project.model.sitemap.UrlModel;
import com.project.repository.JobsImportRepository;
import com.project.repository.SiteMapRepository;
import com.sun.xml.bind.marshaller.DataWriter;

@Controller
public class HomeController
{
    @Autowired
    JobsImportRepository jobsImportRepository;

    @Autowired
    SiteMapRepository siteMapRepository;

    @Value("${db.fetch.limit}")
    int limit;

    @Value("${db.fetch.max.limit}")
    int maxLimit;

    @Autowired
    MongoOperations mongoOperations;

    public void startProcess() {

        int start = 0;

        List<JobsModel> jobsModels = jobsImportRepository.getJobsFromDbWithStartId(start, limit);

        DataWriter dataWriter = siteMapRepository.writeStartElement();

        Marshaller marshaller = siteMapRepository.createMarshaller();

        while ((jobsModels != null) && (jobsModels.size() > 0)) {

            List<JobsDetailMongoDbModel> jobsDetailMongoDbModels = jobsImportRepository.convertJobsDbModelsIntoMongoDbJobsModels(jobsModels);
//
            jobsDetailMongoDbModels = jobsImportRepository.combineTitleLocationForUrlBeautify(jobsDetailMongoDbModels);
//
//            mongoOperations.insert(jobsDetailMongoDbModels, JobsDetailMongoDbModel.class);

            List<UrlModel> urlModelList = siteMapRepository.convertJobModelINtoSitemapUrlModel(jobsDetailMongoDbModels);

            for (UrlModel urlModel : urlModelList) {

                siteMapRepository.writeFragmentedMarshlingForJujuExportModel(urlModel, dataWriter, marshaller);

            }

            start += limit;
            if (start >= maxLimit) {
                break;
            }
            jobsModels = jobsImportRepository.getJobsFromDbWithStartId(start, limit);
        }

        siteMapRepository.writeXmlEndElement(dataWriter);

    }
}
