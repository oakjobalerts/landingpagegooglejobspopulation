package com.project.model.sitemap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "url")
@XmlAccessorType(XmlAccessType.FIELD)
public class UrlModel {

    String loc;
    String lastmod;
    String changefreq;
    float priority;

    public String getLoc() {
        return loc;
    }

    public String getLastmod() {
        return lastmod;
    }

    public String getChangefreq() {
        return changefreq;
    }

    public float getPriority() {
        return priority;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public void setLastmod(String lastmod) {
        this.lastmod = lastmod;
    }

    public void setChangefreq(String changefreq) {
        this.changefreq = changefreq;
    }

    public void setPriority(float priority) {
        this.priority = priority;
    }

    public UrlModel() {
        super();
    }

}
// <url xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
// <loc>http://www.monsterindia.com/landscape-gardening-jobs-in-pune.html</loc>
// <lastmod>2017-02-10</lastmod>
// <changefreq>weekly</changefreq>
// <priority>0.5</priority>
// </url>