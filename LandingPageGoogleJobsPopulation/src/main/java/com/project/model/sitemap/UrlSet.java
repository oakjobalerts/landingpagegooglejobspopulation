package com.project.model.sitemap;

import java.util.List;

//@XmlRootElement(name = "urlset")
//@XmlType(namespace = "http://www.sitemaps.org/schemas/sitemap/0.9")
public class UrlSet {

    List<UrlModel> url;

}

// <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
// xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
// <url>
// <loc>http://www.monsterindia.com</loc>
// <lastmod>2017-02-10</lastmod>
// <changefreq>monthly</changefreq>
// <priority>1.0</priority>
// </url>