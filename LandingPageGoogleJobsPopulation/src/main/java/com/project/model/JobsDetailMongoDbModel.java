package com.project.model;

import java.io.Serializable;
import java.util.Date;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "JobsDetail")
public class JobsDetailMongoDbModel implements Serializable {

	private static final long serialVersionUID = 2000L;

	Number id;

	public Number getId() {
		return id;
	}

	public void setId(Number id) {
		this.id = id;
	}

	String title;
	String city;
	String state;
	String zipcode;
	String description;
	String employer;
	String joburl;
	String postingDate;
	Date createdAt;
	String sourceName;

	@Indexed
	String beautifyUrl;

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getBeautifyUrl() {
		return beautifyUrl;
	}

	public void setBeautifyUrl(String beautifyUrl) {
		this.beautifyUrl = beautifyUrl;
	}

	public String getTitle()
	{
		return title;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public String getDescription() {
		return description;
	}

	public String getEmployer() {
		return employer;
	}

	public String getJoburl() {
		return joburl;
	}

	public String getPostingDate() {
		return postingDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public void setJoburl(String joburl) {
		this.joburl = joburl;
	}

	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public JobsDetailMongoDbModel() {
	}

	public static long getSerialversionuid()
	{
		return 2000L;
	}

	
}
