package com.project.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class JobsModel implements Serializable {

	private static final long serialVersionUID = 1000L;
	String title;
	String category;
	String city;
	String state;
	String zipcode;
	String sourceName;
	String cpc;
	String jobType;
	String description;
	String employer;
	String joburl;
	String postingDate;
	String gcpc;
	Number id;

	@Temporal(TemporalType.DATE)
	Date createdAt;

	public Number getId() {
		return id;
	}

	public void setId(Number id) {
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public String getCategory() {
		return category;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public String getSourceName() {
		return sourceName;
	}

	public static long getSerialversionuid() {
		return 1000L;
	}

	public String getCpc() {
		return cpc;
	}

	public String getJobType() {
		return jobType;
	}

	public String getDescription() {
		return description;
	}

	public String getEmployer() {
		return employer;
	}

	public String getJoburl() {
		return joburl;
	}

	public String getPostingDate() {
		return postingDate;
	}

	public String getGcpc() {
		return gcpc;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public void setJoburl(String joburl) {
		this.joburl = joburl;
	}

	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	public void setGcpc(String gcpc) {
		this.gcpc = gcpc;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public JobsModel() {
	}
}
