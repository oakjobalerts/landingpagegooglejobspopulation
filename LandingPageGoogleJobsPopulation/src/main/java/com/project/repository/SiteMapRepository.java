package com.project.repository;

import java.util.List;

import javax.xml.bind.Marshaller;

import com.project.model.JobsDetailMongoDbModel;
import com.project.model.sitemap.UrlModel;
import com.sun.xml.bind.marshaller.DataWriter;

public interface SiteMapRepository {

    List<UrlModel> convertJobModelINtoSitemapUrlModel(List<JobsDetailMongoDbModel> jobsDetailMongoDbModelList);

    void writeFragmentedMarshlingForJujuExportModel(UrlModel urlModel, DataWriter dataWriter, Marshaller marshaller);

    Marshaller createMarshaller();

    void writeXmlEndElement(DataWriter dataWriter);

    DataWriter writeStartElement();

}
