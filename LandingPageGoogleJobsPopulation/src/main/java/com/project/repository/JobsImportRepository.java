package com.project.repository;

import com.project.model.JobsDetailMongoDbModel;
import com.project.model.JobsModel;
import java.util.List;

public abstract interface JobsImportRepository {

    public abstract List<JobsModel> getJobsFromDbWithStartId(int paramInt1, int paramInt2);

    public abstract List<JobsDetailMongoDbModel> convertJobsDbModelsIntoMongoDbJobsModels(List<JobsModel> paramList);

    List<JobsDetailMongoDbModel> combineTitleLocationForUrlBeautify(List<JobsDetailMongoDbModel> jobsDetailMongoDbModels);
}
